// Configure the Vultr provider.
// Alternatively, export the API key as an environment variable: `export VULTR_API_KEY=<your-vultr-api-key>`.
provider "vultr" {
  api_key = "${var.vultr_api_key}"
}

// Find the ID of the London region.
data "vultr_region" "london" {
  filter {
    name   = "name"
    values = ["London"]
  }
}

// Find the ID for Debian Linux.
data "vultr_os" "debian" {
  filter {
    name   = "family"
    values = ["debian"]
    name = "name"
    values = ["Debian 9 x64 (stretch)"]
    }
}

// Find the ID for a starter plan.
data "vultr_plan" "starter" {
  filter {
    name   = "price_per_month"
    values = ["5.00"]
  }

  filter {
    name   = "ram"
    values = ["1024"]
  }
}

// Find the ID of an existing SSH key.
data "vultr_ssh_key" "ssh_key" {
  filter {
    name   = "name"
    values = ["${var.vultr_ssh_key}"]
  }
}

resource "vultr_instance" "kenny_mccormick" {
  name              = "${var.vultr_vm_name1}"
  region_id         = "${data.vultr_region.london.id}"
  plan_id           = "${data.vultr_plan.starter.id}"
  os_id             = "${data.vultr_os.debian.id}"
  ssh_key_ids       = ["${data.vultr_ssh_key.ssh_key.id}"]
  hostname          = "${var.vultr_vm_name1}"
  tag               = "debian"
  firewall_group_id = "${vultr_firewall_group.firewall1.id}"

}

// Create a new firewall group.
resource "vultr_firewall_group" "firewall1" {
  description = "firewall1"
}

// Add a firewall rule to the group allowing SSH access.
resource "vultr_firewall_rule" "ssh" {
  firewall_group_id = "${vultr_firewall_group.firewall1.id}"
  cidr_block        = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
}