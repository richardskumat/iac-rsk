# Vultr vm creation with Terraform

This folder is just a POC to create a simple VM on vultr.com with terraform.

Example code to run this would be:

```
wget https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_linux_amd64.zip
unzip terraform_0.11.14_linux_amd64.zip
rm -f terraform_0.11.14_linux_amd64.zip
wget https://github.com/squat/terraform-provider-vultr/releases/download/v0.1.9/terraform-provider-vultr_v0.1.9_linux_amd64.tar.gz
tar xvf terraform-provider-vultr_v0.1.9_linux_amd64.tar.gz
rm -f terraform-provider-vultr_v0.1.9_linux_amd64.tar.gz
mkdir -p .terraform/plugins/linux_amd64
mv terraform-provider-vultr_v0.1.9 .terraform/plugins/linux_amd64/
./terraform init
./terraform plan
./terraform apply
```

Community terraform mod for vultr:

https://github.com/squat/terraform-provider-vultr

Release page:

https://github.com/squat/terraform-provider-vultr/releases

https://github.com/squat/terraform-provider-vultr/releases/download/v0.1.9/terraform-provider-vultr_v0.1.9_linux_amd64.tar.gz

20190605 note:

For the time being, this mod's not compatible with 0.12.*

Release binaries:

https://releases.hashicorp.com/terraform/

Linux amd64:

https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_linux_amd64.zip

Vultr api link:

https://www.vultr.com/api/

OS list:

curl https://api.vultr.com/v1/os/list

Price list:

https://www.vultr.com/api/#plans_plan_list_vc2

curl https://api.vultr.com/v1/plans/list?type=vc2

Regions:

https://www.vultr.com/api/#regions

curl https://api.vultr.com/v1/regions/availability_vc2?DCID=1


