variable "vultr_api_key" {
  description = "API key in https://my.vultr.com/settings/#settingsapi"
}

variable "vultr_ssh_key" {
  description = "SSH key to be used"
}

variable "vultr_vm_name1" {
  description = "Disposable vm name"
}
