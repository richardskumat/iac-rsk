# Configure the Cloudflare provider
provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "cloudflare_zone" "domain1" {
    zone = "${var.cloudflare_domain1}"
    paused = "true"
    plan = "free"
    type = "full"
}



# Add a record to the domain
resource "cloudflare_record" "dm1_rootdomain" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "@"
  value  = "127.0.0.1"
  type   = "A"
  ttl    = "${var.default_ttl}"
}


# Add a record to the domain
resource "cloudflare_record" "dm1_ovh2subwildcard" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "${var.dm1_ovh_subdomain}"
  value  = "${var.ovh2_ip}"
  type   = "A"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm1_ovhemailcheck" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "${var.ovh_email_check}"
  value  = "ovh.com"
  type   = "CNAME"
  ttl    = "${var.default_ttl}"
}

# Add *hl.@ record to the domain
resource "cloudflare_record" "dm1_hl_sub" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "*.hl"
  value  = "192.168.0.11"
  type   = "A"
  ttl    = "${var.default_ttl}"
}

# Add *ws.@ record to the domain
resource "cloudflare_record" "dm1_wssub" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "*.ws"
  value  = "192.168.0.21"
  type   = "A"
  ttl    = "${var.default_ttl}"
}

resource "cloudflare_record" "dm1_acsub" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "*.ac"
  value  = "192.168.0.16"
  type   = "A"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm1_wwwsub" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "www"
  value  = "${var.cloudflare_domain1}"
  type   = "CNAME"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm1_mx1" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "@"
  value  = "mx0.mail.ovh.net"
  type   = "MX"
  priority = "1"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm1_mx2" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "@"
  value  = "mx1.mail.ovh.net"
  type   = "MX"
  priority = "5"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm1_mx3" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "@"
  value  = "mx2.mail.ovh.net"
  type   = "MX"
  priority = "50"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm1_mx4" {
  domain = "${cloudflare_zone.domain1.zone}"
  name   = "@"
  value  = "mx3.mail.ovh.net"
  type   = "MX"
  priority = "100"
  ttl    = "${var.default_ttl}"
}

# Add a record requiring a data map
resource "cloudflare_record" "autodiscover_tcp" {
  domain = "${var.cloudflare_domain1}"
  name   = "_autodiscover._tcp"
  type   = "SRV"

  data = {
    service  = "_autodiscover"
    proto    = "_tcp"
    name     = "ovh-email-srv"
    priority = 0
    weight   = 0
    port     = 443
    target   = "${var.ovh_email_autodiscover_srv_target}"
  }
}
