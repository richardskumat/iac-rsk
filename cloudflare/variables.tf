# Provider configuration variables
variable "cloudflare_email" {
  description = "Email account for CF"
}

variable "cloudflare_token" {
  description = "CF API key"
}

# Cluster configuration variables
variable "cloudflare_domain1" {
  description = "The first domain I'm using"
}

variable "cloudflare_domain2" {
  description = "The second domain I'm using"
}

variable "ovh2_ip" {
    description = "VM IP at ovh"
}

variable "ovh_email_check" {
  description = "OVH email pro dns check record"
}

variable "default_ttl" {
  description = "default ttl value"
}

variable "dm1_ovh_subdomain" {
  description = "random subdomain name in use"
}

variable "ovh_email_autodiscover_srv_target" {
  description = "Autodiscover tcp DNS record"
}



