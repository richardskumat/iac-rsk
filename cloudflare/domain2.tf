resource "cloudflare_zone" "domain2" {
    zone = "${var.cloudflare_domain2}"
    paused = "true"
    plan = "free"
    type = "full"
}

# Add a record to the domain
resource "cloudflare_record" "dm2_rootdomain" {
  domain = "${cloudflare_zone.domain2.zone}"
  name   = "@"
  value  = "127.0.0.1"
  type   = "A"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm2_wwwsub" {
  domain = "${cloudflare_zone.domain2.zone}"
  name   = "www"
  value  = "${var.cloudflare_domain2}"
  type   = "CNAME"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm2_mx1" {
  domain = "${cloudflare_zone.domain2.zone}"
  name   = "@"
  value  = "mx.zoho.com"
  type   = "MX"
  priority = "10"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm2_mx2" {
  domain = "${cloudflare_zone.domain2.zone}"
  name   = "@"
  value  = "mx2.zoho.com"
  type   = "MX"
  priority = "20"
  ttl    = "${var.default_ttl}"
}

# Add a record to the domain
resource "cloudflare_record" "dm2_mx3" {
  domain = "${cloudflare_zone.domain2.zone}"
  name   = "@"
  value  = "mx3.zoho.com"
  type   = "MX"
  priority = "50"
  ttl    = "${var.default_ttl}"
}