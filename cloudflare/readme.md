# Terraform and Cloudflare

Done with TF 0.12.1 as of 2019-06-05.

I've moved over my 2 domains that I use from managing by hand at Cloudflare by trying out doing
it with Terraform.

The only real issue I've met was because the records/domains existed at Cloudflare already,
I was getting 400 errors saying xyz.tld is existing.

So I've exported the DNS records from CF, converted them to terraform resources and reapplied
them with success.

I understand with a large enough zone file, manually converting records to terraform resources would be
too manual.

Resources used:

https://www.terraform.io/docs/providers/cloudflare/r/zone.html

https://www.terraform.io/docs/providers/cloudflare/r/record.html

https://www.terraform.io/docs/providers/cloudflare/index.html